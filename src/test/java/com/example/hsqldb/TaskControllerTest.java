package com.example.hsqldb;

import com.example.hsqldb.controller.TaskController;
import com.example.hsqldb.model.TaskEntity;
import com.example.hsqldb.servise.TaskService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(TaskController.class)
public class TaskControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TaskService taskService;

   /* @Test
    public void shouldReturnTheTaskById() throws Exception {
        TaskEntity entity  = new TaskEntity();
        when(taskService.findEntityById(1)).thenReturn(Optional.of(entity));

        this.mockMvc.perform(get("/task/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", Matchers.is(1)))
                .andExpect(jsonPath("$.task", Matchers.is("drink coffee")))
                .andExpect(jsonPath("$.place", Matchers.is("home")))
                .andExpect(jsonPath("$.status", Matchers.is("DONE")))
                .andExpect(jsonPath("$.time_of_creation", Matchers.is("2022-05-08 12:35:29")))
                .andExpect(jsonPath("$.completion_time", Matchers.is(null)));
    }*/

}
